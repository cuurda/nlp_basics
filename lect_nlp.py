# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 09:45:46 2019

@author: User
"""
# will be used later to process NL data
import os

import gensim
from nltk.util import pr
import numpy
import pymorphy2
# needed for service porpuse (dose not matter for business logic understanding)
from nltk.data import find

'''
 will be used to process NL sentences producing list of tuples like [('Украинский', 'ADJ'), ('бок.... ]
 must be trained before use
'''
from nltk.tag.perceptron import PerceptronTagger

'''
named constant which defines directory to store variables binary dumps
dumping variable values could help to avoid reapplying greedy computations on each launch 
'''
BIN_DUMP_DIR = 'bin_dumps/'
LOAD_DUMP = 1
# would be used to make aforementioned binary dumps
import pickle
# would be used to represent text data from "lenta.ru" corpus
import pandas as pd

# actually would not be used
RUS_PICKLE = 'taggers/averaged_perceptron_tagger_ru/averaged_perceptron_tagger_ru.pickle'
# named constant which defines path to train set witch would be used to setup Part Of Speech(POS) tagger (line:15)
PATH2CORP = dict(zip(['dev', 'train', 'pos'], ['RUPOS/ru_pos_dev', 'RUPOS/ru_pos_train', 'RUPOS/ru_pos_test']))

global pretrained_vector_model, news_vec_dictionary, text_corp, news_back_index, news_vecs, PMA, titles

'''
 create and save a dump of given variable
 l - is value to save in filesystem
 fn - is an alias we would use to reload "l" value
 d - is directory to store dump file (in particular case BIN_DUMP_DIR defined at line 21) 
'''


def store_bin(l, fn, d=BIN_DUMP_DIR): pickle.dump(l, open(d + fn + ".bin", "wb"))


'''
 obtain object from previously stored dump
 fn - is an alias we have used to store object (while call store_bin(%object%,%name%))
 d - is directory where dump files stored (in particular case BIN_DUMP_DIR defined at line 21) 
'''


def restore_bin(fn, d=BIN_DUMP_DIR): return pickle.load(open(d + fn + '.bin', "rb"))


def _get_tagger(lang=None, ts=None):
    '''
        will not be used, defined to provide backward compatibility with older projects
    '''
    if lang == 'rus':
        tagger = PerceptronTagger(False)
        if (ts != None):
            tagger.train(ts)
        else:
            ap_russian_model_loc = 'file:' + str(find(RUS_PICKLE))
            tagger.load(ap_russian_model_loc)
    else:
        tagger = PerceptronTagger()
    return tagger


def train_active(ts=None):
    '''
    When called with specifically structured Training Set (a.k.a ts) witch
    contains NL sentences slitted to single words where each word has given
    (provided by expert or other valid source) POS tag returns Tagger object.
    Trained tagger could forecast POS for given word list (in assumption it is sentence)

    After first call, when training process would be finished, we should make
    dump of returned object with 'store_bin' so in future launches we may skip train procedure
    and simply restore binary dump of trained structure
    '''
    tagger = PerceptronTagger(False)
    if (ts != None):
        print("Start training with " + str(len(ts)) + " of sentences")
        tagger.train(ts)
        print("Training succesfully finished")
    else:
        print("No training set was passed, trying to use default!")
        ap_russian_model_loc = 'file:' + str(find(RUS_PICKLE))
        tagger.load(ap_russian_model_loc)
    set_active(tagger)
    return tagger


def set_active(t):
    '''
        Needed as abstraction layer to be sure we
                                    - got Tagger object
                                    - it is singleton and visible over this script
                                    - it has predefined name 'A_TAGGER'
    '''
    global A_TAGGER
    A_TAGGER = t
    return A_TAGGER


'''
Calling train_active with list of example sentences restored from dump
calculation could take big amount of time
'''


def get_word_normal_form(w): return PMA.parse(w)[0].normal_form


def get_word_vector_or_die(e):
    i = pretrained_vector_model.key_to_index.get(e)
    if i is not None:
        return pretrained_vector_model.vectors[i]
    else:
        return ""


def get_news_vectors(num):
    a = list(filter(lambda x: x[1] in ["ADJ", "NOUN", "VERB"], A_TAGGER.tag(text_corp[num].split('.')[0].split(' '))))
    return list(filter(lambda x: isinstance(x, numpy.ndarray), list(
        map(get_word_vector_or_die, list(map(lambda e: get_word_normal_form(e[0]) + "_" + e[1], list(a)))))))


def get_joined_vector(num):
    zv = numpy.zeros([300], numpy.float32)
    try:
        zv = numpy.asarray(numpy.mat(get_news_vectors(num)).mean(0).tolist()[0], numpy.float32)
    except Exception as e:
        zv.fill(-1)
        print(text_corp[num])
        raise e
    finally:
        return dict(zip([i for i in range(0, 300)], zv))


def get_news_vec_dictionary():
    global news_vecs # = dict(zip([i for i in range(0, len(text_corp))], list(map(get_joined_vector, text_corp))))
    news_vecs = dict(zip([i for i in range(0, len(text_corp))], list(map(get_joined_vector, text_corp))))
    return news_vecs


def describe_news_item(num):
    print(text_corp[num])
    s = pretrained_vector_model.similar_by_vector(get_joined_vector(num), 10)
    print(s)
    # print(get_joined_vector(5))


def get_similar_news_by_id(num, topn=10, print_full=False):
    print(text_corp[num])
    print("*************************")
    pw = dict(zip([i for i in range(0, len(text_corp))],
                  [gensim.matutils.cossim(news_vecs.get(num), others) for others in news_vecs.values()]))
    pw = sorted(pw.items(), key=lambda x: x[1])
    pw.reverse()
    idx = [pw[x][0] for x in range(0, topn)]

    list(map(lambda z:print(z), [titles[x]+(text_corp[x] if print_full else "") for x in idx]))
    return idx


def gsn(n,topn=10):
    return get_similar_news_by_id(n,topn)

def get_similar_news_by_ids(nums, topn=10, print_full=False):
    for num in nums:
        print("*****")
        print(text_corp[num])
        
    print("*************************")
    array_of_vecs = []
    for num in nums:
        array_of_vecs.append([vals for vals in news_vecs.get(num).values()])
    news_mean = dict(enumerate(numpy.array(array_of_vecs).mean(axis=0)))
    pw = dict(zip([i for i in range(0, len(text_corp))],
                  [gensim.matutils.cossim(news_mean, others) for others in news_vecs.values()]))
    pw = sorted(pw.items(), key=lambda x: x[1])
    pw.reverse()
    idx = [pw[x][0] for x in range(0, topn)]

    list(map(lambda z:print(z), [titles[x]+(text_corp[x] if print_full else "") for x in idx]))
    return idx


PMA = pymorphy2.MorphAnalyzer()

if LOAD_DUMP:
    set_active(restore_bin('taggie'))
    news = restore_bin("news")
    pretrained_vector_model = restore_bin("ruwiki_model")
    news_back_index = restore_bin("bi")
    news_vecs = restore_bin("nv")

    news_vec_dictionary = news.to_dict()
    text_corp = news_vec_dictionary.get('text')
    titles=news_vec_dictionary.get('title')

else:
    '''
     for future calls
        line 102: train_active(restore_bin(PATH2CORP['train']))
     should be replaced with
        set_active(restore_bin('taggie'))
    '''
    train_active(restore_bin(PATH2CORP['train']))
    # ASAP save training result to binary dump
    store_bin(A_TAGGER, 'taggie')
    '''
    let read data from large set of news
    then store it with store_bin routine
    '''
    news = pd.read_csv(os.path.join('data', 'news_lenta.csv.bz2'), low_memory=False, iterator=True, chunksize=100000,
                       error_bad_lines=False).read(4000)
    store_bin(news, 'news')
    news_vec_dictionary = news.to_dict()
    text_corp = news_vec_dictionary.get('text')
    titles = news_vec_dictionary.get('title')


    w2vDict = os.path.join('w2v', 'ruwikiruscorpora_upos_skipgram_300_2_2018.vec.gz')
    pretrained_vector_model = gensim.models.KeyedVectors.load_word2vec_format(w2vDict)
    #print(pretrained_vector_model.key_to_index.)
    store_bin(pretrained_vector_model, "ruwiki_model")
    
    news_back_index = list(map(get_joined_vector, [i for i in range(1, news.size)]))

    store_bin(news_back_index, "bi")
    
    get_news_vec_dictionary()
    store_bin(news_vecs, "nv")

#gsn(15, 20)
get_similar_news_by_ids([2, 3], 10)
