# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 00:55:13 2018

@author: Phantom
"""

from __future__ import print_function

from nltk import pos_tag, word_tokenize

from nltk.tag.api           import TaggerI
from nltk.tag.util          import str2tuple, tuple2str, untag
from nltk.tag.sequential    import (SequentialBackoffTagger, ContextTagger,
                                    DefaultTagger, NgramTagger, UnigramTagger,
                                    BigramTagger, TrigramTagger, AffixTagger,
                                    RegexpTagger, ClassifierBasedTagger,
                                    ClassifierBasedPOSTagger)
from nltk.tag.brill         import BrillTagger
from nltk.tag.brill_trainer import BrillTaggerTrainer
from nltk.tag.tnt           import TnT
from nltk.tag.hunpos        import HunposTagger
from nltk.tag.stanford      import StanfordTagger, StanfordPOSTagger, StanfordNERTagger
from nltk.tag.hmm           import HiddenMarkovModelTagger, HiddenMarkovModelTrainer
from nltk.tag.senna         import SennaTagger, SennaChunkTagger, SennaNERTagger
from nltk.tag.mapping       import tagset_mapping, map_tag
from nltk.tag.crf           import CRFTagger
from nltk.tag.perceptron    import PerceptronTagger

from nltk.data import load, find

import sqlite3 as sq3
import sys
import io
import pandas as pd
import numpy as np
#from pymorphy import get_morph

RUS_PICKLE = 'taggers/averaged_perceptron_tagger_ru/averaged_perceptron_tagger_ru.pickle'
EXLS_FILE='d://_WORK//_PAWLIN//inform_model_TXT.xlsx'




def load_sentences(col_name):
    st = ''
    s = pd.read_excel(EXLS_FILE)
    text = []
#    for i, row in s.iterrows():
#        if (isinstance(row[col_name], str)):
#            st = row[col_name]
#        else:
#            st = str(row[col_name])
#        text.append(st)
    return s
    
EF=pd.ExcelFile(EXLS_FILE)
ps=EF.parse( sheet_name='Выборка 10 - финал',
            usecols=18,
    dtype={
        'DUPLICATE':np.bool,
#        'SOURCE_ID':np.int16,
#        'PUB_DATE':,
        'ANALYTICS':np.bool	,
        'AGENTS':np.bool,	
        'URGENT':np.bool,	
        'CLASSIFIED':np.bool},
    converters={
#        'PUB_DATE':(lambda x:np.parse_dates(x))  
            
            }
    )

for cols in ps:
    print(row)


EF.parse()
load_sentences("ff")

def _get_tagger(lang=None):
    if lang == 'rus':
        tagger = PerceptronTagger(False)
        ap_russian_model_loc = 'file:' + str(find(RUS_PICKLE))
        tagger.load(ap_russian_model_loc)
    else:
        tagger = PerceptronTagger()
    return tagger


def _pos_tag(tokens, tagset, tagger):
    tagged_tokens = tagger.tag(tokens)
    if tagset:
        tagged_tokens = [(token, map_tag('en-ptb', tagset, tag)) for (token, tag) in tagged_tokens]
    return tagged_tokens


def pos_tag(tokens, tagset=None, lang='eng'):
    """
    Use NLTK's currently recommended part of speech tagger to
    tag the given list of tokens.

        >>> from nltk.tag import pos_tag
        >>> from nltk.tokenize import word_tokenize
        >>> pos_tag(word_tokenize("John's big idea isn't all that bad."))
        [('John', 'NNP'), ("'s", 'POS'), ('big', 'JJ'), ('idea', 'NN'), ('is', 'VBZ'),
        ("n't", 'RB'), ('all', 'PDT'), ('that', 'DT'), ('bad', 'JJ'), ('.', '.')]
        >>> pos_tag(word_tokenize("John's big idea isn't all that bad."), tagset='universal')
        [('John', 'NOUN'), ("'s", 'PRT'), ('big', 'ADJ'), ('idea', 'NOUN'), ('is', 'VERB'),
        ("n't", 'ADV'), ('all', 'DET'), ('that', 'DET'), ('bad', 'ADJ'), ('.', '.')]

    NB. Use `pos_tag_sents()` for efficient tagging of more than one sentence.

    :param tokens: Sequence of tokens to be tagged
    :type tokens: list(str)
    :param tagset: the tagset to be used, e.g. universal, wsj, brown
    :type tagset: str
    :param lang: the ISO 639 code of the language, e.g. 'eng' for English, 'rus' for Russian
    :type lang: str
    :return: The tagged tokens
    :rtype: list(tuple(str, str))
    """
    tagger = _get_tagger(lang)
    return _pos_tag(tokens, tagset, tagger)    



def pos_tag_sents(sentences, tagset=None, lang='eng'):
    """
    Use NLTK's currently recommended part of speech tagger to tag the
    given list of sentences, each consisting of a list of tokens.

    :param tokens: List of sentences to be tagged
    :type tokens: list(list(str))
    :param tagset: the tagset to be used, e.g. universal, wsj, brown
    :type tagset: str
    :param lang: the ISO 639 code of the language, e.g. 'eng' for English, 'rus' for Russian
    :type lang: str
    :return: The list of tagged sentences
    :rtype: list(list(tuple(str, str)))
    """
    tagger = _get_tagger(lang)
    return [_pos_tag(sent, tagset, tagger) for sent in sentences]

# In[14]:
sentence="Красный Илья оторопел и дважды перечитал бумажку."
sentence="Как заявил Бортников президенту Дмитрию Медведеву, у террористов были изъяты самодельная бомба мощностью 10 килограммов тротила, оружие, карты, схемы места проведения теракта. "
a=pos_tag(word_tokenize(sentence), lang='rus', tagset=None) 
con=None
# In[15]:
def codepageTweek():
    sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding = 'utf-8')
    sys.stderr = io.TextIOWrapper(sys.stderr.detach(), encoding = 'utf-8')

def sqGetEmptyTbl(c):
    conn = sq3.connect("mydatabase.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()
     
    # Создание таблицы
    cursor.execute("""CREATE TABLE rawData
                      ("""+(" text").join(c.columns)+""")
                   """)



def sqNew():
    conn = sq3.connect("mydatabase.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()
     
    # Создание таблицы
    cursor.execute("""CREATE TABLE rawData
                      (title text, artist text, release_date text,
                       publisher text, media_type text)
                   """)
                      
sqNew()

#[print for w in word_tokenize(sentence)]
#print(sentence)
#print(a)
#g=lambda x:x+"_"
#a=[(lambda x:x+"_")(w) for w in word_tokenize(sentence)]
#tagger = _get_tagger('rus')
#tagger.tagdict.__len__()
