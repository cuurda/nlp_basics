import gensim
import numpy
import pandas as pd
import pickle

from nltk.tag.perceptron import PerceptronTagger

A_TAGGER = None
BIN_DUMP_DIR = 'bin_dumps/'
LOAD_DUMP = 1

PATH2CORP = dict(zip(
    ['dev', 'train', 'pos'], 
    ['RUPOS/ru_pos_dev', 'RUPOS/ru_pos_train', 'RUPOS/ru_pos_test'],
))



global pretrained_vector_model, news_vec_dictionary, text_corp, news_back_index, news_vecs, PMA, titles, news

def store_bin(l, file_name, dir_=BIN_DUMP_DIR):
    with open(dir_ + file_name + '.bin', 'wb') as file:
        pickle.dump(l, file)

def restore_bin(file_name, dir_=BIN_DUMP_DIR):
    with open(dir_ + file_name + '.bin', 'rb') as file:
        return pickle.load(file)


def train_active(ts=None):
    '''
    When called with specifically structured Training Set (a.k.a ts) witch
    contains NL sentences slitted to single words where each word has given
    (provided by expert or other valid source) POS tag returns Tagger object.
    Trained tagger could forecast POS for given word list (in assumption it is sentence)

    After first call, when training process would be finished, we should make
    dump of returned object with 'store_bin' so in future launches we may skip train procedure
    and simply restore binary dump of trained structure
    '''
    tagger = PerceptronTagger(False)
    if (ts != None):
        print("Start training with " + str(len(ts)) + " of sentences")
        tagger.train(ts)
        print("Training succesfully finished")
    else:
        print("No training set was passed, trying to use default!")
        raise NotImplemented
    set_active(tagger)
    return tagger


def set_active(t):
    '''
        Needed as abstraction layer to be sure we
            - got Tagger object
            - it is singleton and visible over this script
            - it has predefined name 'A_TAGGER'
    '''
    global A_TAGGER
    A_TAGGER = t
    return A_TAGGER


def get_word_normal_form(w):
    return PMA.parse(w)[0].normal_form

def get_word_vector_or_die(e):
    i = pretrained_vector_model.vocab.get(e)
    if isinstance(i, gensim.models.keyedvectors.Vocab):
        return pretrained_vector_model.vectors[i.index]
    else:
        return ""

def get_news_vectors(num):
    a = list(filter(
        lambda x: x[1] in ["ADJ", "NOUN", "VERB"],
        A_TAGGER.tag(text_corp[num].split('.')[0].split(' ')),
    ))
    return (
        list(filter(
            lambda x: isinstance(x, numpy.ndarray),
            list(map(
                get_word_vector_or_die,
                list(map(lambda e: get_word_normal_form(e[0]) + "_" + e[1], list(a)))
            )),
        ))
    )

def get_joined_vector(num):
    zv = numpy.zeros([300], numpy.float32)
    try:
        zv = numpy.asarray(numpy.mat(get_news_vectors(num)).mean(0).tolist()[0], numpy.float32)
    except:
        zv.fill(-1)
        print(text_corp[num])
    finally:
        return dict(zip([i for i in range(0, 300)], zv))

def main():
    global news
    if LOAD_DUMP:
        set_active(restore_bin('taggie'))
        news = restore_bin('news')
        pretrained_vector_model = restore_bin('ruwiki_model')
    else:
        '''
        for future calls
            line 102: train_active(restore_bin(PATH2CORP['train']))
        should be replaced with
            set_active(restore_bin('taggie'))
        '''
        train_active(restore_bin(PATH2CORP['train']))
        # ASAP save training result to binary dump
        store_bin(A_TAGGER, 'taggie')

        news = pd.read_csv('.\\data\\news_lenta.csv.bz2', low_memory=False, iterator=True, chunksize=100000,
                       error_bad_lines=False).read(4000)
        store_bin(news, 'news')
        
        w2vDict = '.\\w2v\\ruwikiruscorpora_upos_skipgram_300_2_2018.vec.gz'
        pretrained_vector_model = gensim.models.KeyedVectors.load_word2vec_format(w2vDict)
        store_bin(pretrained_vector_model, 'ruwiki_model')

        news_back_index = list(map(get_joined_vector, [i for i in range(1, news.size)]))
        store_bin(news_back_index, "bi")



if __name__ == '__main__':
    main()
    #Глава ЦИК Памфилова: более шести тысяч бюллетеней признаны недействительными
